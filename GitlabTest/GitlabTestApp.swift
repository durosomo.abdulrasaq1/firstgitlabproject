//
//  GitlabTestApp.swift
//  GitlabTest
//
//  Created by Abdulrasaq on 06/05/2023.
//

import SwiftUI

@main
struct GitlabTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
